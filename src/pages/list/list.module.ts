import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from 'ionic-angular';
import { AgmCoreModule } from '@agm/core';

import { ListPage } from './list.component';
import { AddMarkerComponent } from '../../components/add-marker/add-marker.component';
import { MarkerDataModule } from '../../components/marker-data/marker-data.module';

@NgModule({
  declarations: [
    ListPage,
    AddMarkerComponent
  ],
  imports: [
    CommonModule,
    IonicModule,
    AgmCoreModule.forRoot({
      apiKey: "AIzaSyAAUnJBF_GNRTyrq71JshhFOP3YW5mmSRU",
      apiVersion: '3.31',
      language: 'en',
      libraries: ['geometry', 'places']
    }),
    MarkerDataModule
  ],
  entryComponents: [
    ListPage,
    AddMarkerComponent
  ]
})
export class ListModule {}
