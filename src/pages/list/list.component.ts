import {
  Component,
  OnInit,
  ViewChild
} from '@angular/core';
import { Modal } from 'ionic-angular';
import {
  AgmInfoWindow,
  AgmMap
} from '@agm/core';

import { MarkerModel } from '../../models/marker.model';
import { ApiService } from '../../services/api.service';
import { ModalService } from '../../services/modal.service';
import { AddMarkerComponent } from '../../components/add-marker/add-marker.component';
import { MapsService } from '../../services/maps.service';

@Component({
  selector: 'page-list',
  templateUrl: 'list.component.html'
})
export class ListPage implements OnInit {

  @ViewChild('map') mapElement: AgmMap;
  @ViewChild('infoWindow') infoWindow: any;
  public markers: MarkerModel[];
  public isLoading: boolean;
  private lastInfoWindow: AgmInfoWindow;

  constructor(
    private apiService: ApiService,
    private modalService: ModalService,
    private mapsService: MapsService
  ) {
    this.isLoading = true;
    this.markers = [];
  }

  public ngOnInit(): void {
    this.apiService.list().then(
      (markers: MarkerModel[]) => {
        this.markers = markers;
        this.isLoading = false;
      },
      (error) => {
        console.log(error);
      }
    );
  }

  public markerClicked(infoWindow: AgmInfoWindow) {
    this.closeInfoWindow();
    this.lastInfoWindow = infoWindow;
  }

  public clickAdd(): void {
    this.closeInfoWindow();
    this.showGoogleLocationSuggest().then(
      (newMarker: MarkerModel) => {
        this.markers.push(newMarker);
        this.isLoading = false;
      },
      (error) => {
        console.log(error);
        this.isLoading = false;
      }
    );
  }

  public removeMarker(id: number): void {
    this.apiService.remove(id).subscribe(
      (res) => {
        if (res.status === 204) {
          this.markers = this.removeStateItemById(id, this.markers);
          this.lastInfoWindow = null;
        }
      },
      (error) => {
        console.log(error);
      }
    )
  }

  public showGoogleLocationSuggest(): Promise<MarkerModel> {
    return new Promise(
      (resolve, reject) => {
        const modalSearchLocation = this.modalService.presentModal(AddMarkerComponent);

        modalSearchLocation.then(
          (modal: Modal) => {
            modal.isOverlay = true;
            const searchComponent = modal.overlay['instance'] as AddMarkerComponent;

            this.mapsService.linkAddMarkerToGmap(searchComponent).then(
              (newMarker: MarkerModel) => {
                this.modalService.dismissModal();
                this.isLoading = true;
                this.apiService.getImage(newMarker.content).then(
                  (image: string) => {
                    newMarker.image_url = image;
                    this.apiService.save(newMarker).subscribe(
                      (res) => {
                        if (res.status === 201) {
                          newMarker.id = res.body.id;
                          resolve(newMarker);
                        } else {
                          reject('Error: ' + res.status);
                        }

                      },
                      (error) => {
                        reject(error)
                      }
                    );
                  },
                  (error) => {
                    console.log(error);
                  }
                );
              }
            )
          }
        );
      }
    );
  }

  private removeStateItemById(id: number, markers: MarkerModel[]): MarkerModel[] {
    return markers.filter((elem) => { return elem.id !== id });
  }

  private closeInfoWindow(): void {
    if (this.lastInfoWindow) {
      this.lastInfoWindow.close();
    }
  }

}
