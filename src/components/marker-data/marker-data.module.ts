import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from 'ionic-angular';

import { MarkerDataComponent } from './marker-data.component';
import { ApiService } from '../../services/api.service';

@NgModule({
  declarations: [
    MarkerDataComponent
  ],
  imports: [
    CommonModule,
    IonicModule
  ],
  providers: [
    ApiService
  ],
  exports: [
    MarkerDataComponent
  ]
})
export class MarkerDataModule {}
