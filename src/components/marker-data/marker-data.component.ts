import {
  Component,
  Input,
  Output,
  EventEmitter
} from '@angular/core';

import { MarkerModel } from '../../models/marker.model';
import { ApiService } from '../../services/api.service';

@Component({
  selector: 'marker-data',
  templateUrl: 'marker-data.component.html'
})
export class MarkerDataComponent {

  @Input() marker: MarkerModel;
  @Output() remove: EventEmitter<number>;
  public isEditing: boolean;
  private markerBackup: MarkerModel;

  constructor (private apiService: ApiService) {
    this.isEditing = false;
    this.remove = new EventEmitter();
  }

  public ngOnInit(): void {
    this.markerBackup = this.detachedCopy(this.marker);
  }

  public toggleEditMode(): void {
    this.isEditing = !this.isEditing;
  }

  public clickUpdate(marker: MarkerModel): void {
    this.apiService.update(marker).subscribe(
      (res) => {
        if (res.status === 204) {
          this.marker = this.detachedCopy(this.markerBackup);
          this.isEditing = false;
        }
      }
    );
  }

  public clickCancel(): void {
    this.markerBackup = this.detachedCopy(this.marker);
    this.isEditing = false;
  }

  public clickRemove(id: number): void {
    this.remove.emit(id);
  }

  private detachedCopy(elem: any): any {
    return Object.create(elem);
  }
}
