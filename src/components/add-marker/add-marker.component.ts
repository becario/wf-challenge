import {
    Component,
    ViewChild
} from '@angular/core';
import {ModalService} from '../../services/modal.service';

@Component({
    selector: 'trip-add-marker',
    templateUrl: 'add-marker.component.html'
})
export class AddMarkerComponent {

    public locationName: string;
    @ViewChild('gmapsLocation') public gmapsLocation;

    constructor(
        private modalService: ModalService
    ) {
    }

    public closeModal(): void {
        this.modalService.dismissModal();
    }

}
