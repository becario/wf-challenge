export class MarkerModel {
  public id: number;
  public title: string;
  public content: string;
  public lat: number;
  public long: number;
  public image_url: string;

  constructor(args?: any) {
    Object.assign(this, ...args);
  }
}
