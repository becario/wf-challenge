import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { MarkerModel } from '../models/marker.model';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class ApiService {

  private challengeUrl: string;

  constructor(
    private http: HttpClient
  ) {
    this.challengeUrl = 'https://wf-challenge-ceanyth.herokuapp.com/posts/';
  }

  public list(): Promise<MarkerModel[]> {
    return new Promise(
      (resolve, reject) => {
        this.http.get(this.challengeUrl).subscribe(
          (response: MarkerModel[]) => {
            resolve(this.castItems(response));
          },
          (error) => {
            reject(error);
          }
        );
      }
    );
  }

  public save(marker: MarkerModel): Observable<any> {
    return this.http.post(this.challengeUrl, { post: marker }, {observe: 'response'});
  }

  public remove(id: number): Observable<any> {
    if (id) {
      return this.http.delete(this.challengeUrl + id, {observe: 'response'});
    } else {
      return Observable.of(null);
    }
  }

  public update(marker: MarkerModel): Observable<any> {

    return this.http.put(this.challengeUrl + marker.id, { post: marker }, { observe: 'response'} );
  }

  private castItems(rawItems: any[]): MarkerModel[] {
    return rawItems.map((elem) => {
      return new MarkerModel({
        id: elem.id,
        title: elem.title,
        content: elem.content,
        lat: parseFloat(elem.lat),
        long: parseFloat(elem.long),
        image_url: elem.image_url
      });
    });
  }

  public getImage(query: string): Promise<string> {
    return new Promise(
      (resolve, reject) => {
        const url = 'https://www.googleapis.com/customsearch/v1?key=AIzaSyAAUnJBF_GNRTyrq71JshhFOP3YW5mmSRU&cx=013673665645702458760:f3hxy-vwhv8&q=' + encodeURI(query) + '&searchType=image&fileType=jpg&alt=json';

        this.http.get(url).subscribe(
          (res: any) => { resolve(res.items[0].link) },
          (error) => { reject(error) }
        )
      }
    );

  }

}
