import {Injectable} from '@angular/core';
import {
  Modal,
  ModalController
} from 'ionic-angular';

@Injectable()
export class ModalService {

  public modal: Modal;

  constructor(
    public modalCtrl: ModalController
  ) {}

  public presentModal(page: any): Promise<Modal> {
    return new Promise((resolve) => {
      this.modal = this.modalCtrl.create(page);
      this.modal.present().then(
        () => {
          resolve(this.modal);
        }
      );
    });
  }

  public dismissModal(): void {
    this.modal.dismiss();
  }

}
