import {
  Injectable,
  NgZone
} from '@angular/core';

import {MarkerModel} from '../models/marker.model';
import {AddMarkerComponent} from '../components/add-marker/add-marker.component';

@Injectable()
export class MapsService {

  constructor(
    private ngZone: NgZone
  ) {

  }

  public linkAddMarkerToGmap(component: AddMarkerComponent): Promise<MarkerModel> {
    return new Promise(
      (resolve) => {
        const elem = component.gmapsLocation._elementRef.nativeElement.querySelector('input');
        const autocomplete = new google.maps.places.Autocomplete(elem);

        autocomplete.addListener('place_changed', () => {
          this.ngZone.run(() => {
            let place: google.maps.places.PlaceResult = autocomplete.getPlace();

            //verify result
            if (place.geometry === undefined || place.geometry === null) {
              return;
            }

            resolve(this.getNewMarker(place));
          });
        });
      }
    );
  }

  private getNewMarker(response: google.maps.places.PlaceResult): MarkerModel {
    return new MarkerModel({
      title: response.name,
      content: response.formatted_address,
      lat: response.geometry.location.lat(),
      long: response.geometry.location.lng(),
      image_url: null
    });
  }
}
